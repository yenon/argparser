package yenon.commands

class ArgParser : CommandPropagator() {
    override fun initialize() {}

    override fun checkSelf(args: MutableList<String>): Boolean {
        return true
    }

    override fun executeSelf(args: MutableList<String>): MutableList<String> {
        return args
    }
}

fun Array<out String>.parseArgs(init: ArgParser.() -> Unit) {
    val parser = ArgParser()
    parser.init()

    parser.propagate(this.toMutableList())
}

fun MutableList<String>.parseArgs(init: ArgParser.() -> Unit) {
    val parser = ArgParser()
    parser.init()
    parser.propagate(this)
}
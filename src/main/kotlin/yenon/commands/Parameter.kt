package yenon.commands

import java.lang.RuntimeException

class Parameter(longName: String, private val shortName: Char? = null, val effect: Parameter.() -> Unit) :
    CommandPropagator() {

    private val startLong = Regex("""^--$longName(?<type>$|=)(?<command>.*)$""")
    private val startShort = if (shortName == null) {
        null
    } else {
        Regex("""^-[^-]*?$shortName(?<command>$|.*$)""")
    }

    override fun initialize() {
        this.effect()
    }

    private var effectCallback: ((String, MutableList<String>) -> Unit)? = null

    fun effect(callback: (String) -> Unit) {
        effectCallback = { value, _ ->
            callback.invoke(value)
        }
    }

    fun effectWithRemaining(callback: (value: String, remaining: MutableList<String>) -> Unit) {
        effectCallback = callback
    }

    override fun checkSelf(args: MutableList<String>): Boolean {
        return args.any {
            startLong.containsMatchIn(it) || startShort?.containsMatchIn(it) ?: false
        }
    }

    override fun executeSelf(args: MutableList<String>): MutableList<String> {
        var first: String? = null
        val iterator = args.listIterator()
        while (iterator.hasNext()) {
            val current = iterator.next()
            startLong.find(current)?.let {
                if (it.groups["type"]!!.value == "=") {
                    first = it.groups["command"]!!.value
                    iterator.remove()
                } else {
                    if (iterator.hasNext()) {
                        iterator.remove()
                        val res = iterator.next()
                        iterator.remove()
                        if (first == null) {
                            first = res
                        }
                    } else {
                        throw RuntimeException("Not enough arguments!")
                    }
                }
            }
            startShort?.find(current)?.let {
                if (it.groups["command"]!!.value == "") {
                    val new = current.substringBefore(shortName!!)
                    if (new == "-") {
                        iterator.remove()
                    } else {
                        iterator.set(new)
                    }
                    if (iterator.hasNext()) {
                        if (iterator.hasNext()) {
                            val res = iterator.next()
                            iterator.remove()
                            if (first == null) {
                                first = res
                            }
                        }
                    } else {
                        throw RuntimeException("Not enough arguments!")
                    }
                } else {
                    if (first == null) {
                        first = it.groups["command"]!!.value
                    }
                    val ret = current.substringBeforeLast(shortName!!)
                    if (ret == "-") {
                        iterator.remove()
                    } else {
                        iterator.set(ret)
                    }
                }
            }
        }

        first?.let {
            effectCallback?.invoke(it, args)
        }
        return args
    }
}
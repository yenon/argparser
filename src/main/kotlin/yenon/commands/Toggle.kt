package yenon.commands

class Toggle(longName: String, private val shortName: Char? = null, val effect: Toggle.() -> Unit) :
    CommandPropagator() {

    private val startLong = Regex("""^--$longName$""")
    private val startShort = if (shortName == null) {
        null
    } else {
        Regex("""^-[^-]*$shortName.*$""")
    }

    override fun initialize() {
        this.effect()
    }

    private var effectCallback: ((remaining: MutableList<String>) -> Unit)? = null

    fun effect(callback: () -> Unit) {
        effectCallback = {
            callback()
        }
    }

    fun effectWithRemaining(callback: (remaining: MutableList<String>) -> Unit) {
        effectCallback = callback
    }

    override fun checkSelf(args: MutableList<String>): Boolean {
        return args.any {
            startLong.containsMatchIn(it) || startShort?.containsMatchIn(it) ?: false
        }
    }

    override fun executeSelf(args: MutableList<String>): MutableList<String> {
        var found = false
        args.mapInPlace { _, it ->
            if (startLong.containsMatchIn(it)) {
                found = true
                return@mapInPlace null
            }
            if (startShort?.containsMatchIn(it) == true) {
                found = true
                val new = it.replace("$shortName", "")
                if (new == "-") {
                    return@mapInPlace null
                } else {
                    return@mapInPlace new
                }
            }
            return@mapInPlace it
        }

        if (found) {
            effectCallback?.invoke(args)
        }

        return args
    }
}
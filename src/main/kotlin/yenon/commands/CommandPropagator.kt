package yenon.commands

abstract class CommandPropagator {
    private val nestedPropagators = arrayListOf<CommandPropagator>()

    private var init = false
    abstract fun initialize()

    fun toggle(longName: String, shortName: Char? = null, effect: Toggle.() -> Unit) {
        nestedPropagators.add(Toggle(longName, shortName, effect))
    }

    fun parameter(longName: String, shortName: Char? = null, effect: Parameter.() -> Unit) {
        nestedPropagators.add(Parameter(longName, shortName, effect))
    }

    abstract fun checkSelf(args: MutableList<String>): Boolean

    abstract fun executeSelf(args: MutableList<String>): MutableList<String>

    fun propagate(args: MutableList<String>): MutableList<String> {
        var copyArgs = args

        if (checkSelf(copyArgs)) {
            if (!init) {
                initialize()
            }
            nestedPropagators.forEach {
                copyArgs = it.propagate(copyArgs)
            }
            copyArgs = executeSelf(copyArgs)
        }

        return copyArgs
    }

    inline fun <T> MutableList<T>.mapInPlace(mutator: (MutableListIterator<T>, T) -> T?) {
        val iterate = this.listIterator()
        while (iterate.hasNext()) {
            val oldValue = iterate.next()
            val newValue = mutator(iterate, oldValue)
            if (newValue == null) {
                iterate.remove()
            } else {
                if (newValue !== oldValue) {
                    iterate.set(newValue)
                }
            }
        }
    }
}
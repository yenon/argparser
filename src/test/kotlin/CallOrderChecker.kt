import org.junit.jupiter.api.Assertions

object CallOrderChecker {
    class CallOrderChecker(val size: IntRange) {
        var current = 0

        fun call(pos: Int) {
            if (current + 1 == pos) {
                current++
            } else {
                Assertions.assertTrue(false, "expected call ${current + 1}, got call $pos")
            }
        }

        fun checkCompletion() {
            Assertions.assertTrue(current in size, "Calls did not reach required range.")
        }
    }

    fun create(size: Int, checker: CallOrderChecker.() -> Unit) {
        val checkerObj = CallOrderChecker(size..size)
        checkerObj.checker()
        checkerObj.checkCompletion()
    }

    fun create(size: IntRange, checker: CallOrderChecker.() -> Unit) {
        val checkerObj = CallOrderChecker(size)
        checkerObj.checker()
        checkerObj.checkCompletion()
    }
}


import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import yenon.commands.parseArgs
import java.lang.RuntimeException

class CommandTest {
    @Test
    fun testToggle() {
        CallOrderChecker.create(3) {
            arrayOf("--test", "--ing", "-i", "hello").parseArgs {
                toggle("test") {
                    toggle("ing") {
                        effect {
                            call(1)
                        }
                    }
                    parameter("in", 'i') {
                        effect {
                            call(2)
                        }
                    }
                    effect {
                        call(3)
                    }
                }
            }
        }
    }

    @Test
    fun testAll() {
        CallOrderChecker.create(5) {
            arrayOf("--server=10", "--get", "file.txt", "--encoding=utf-8", "--verbose").parseArgs {
                parameter("server") {
                    effect {
                        if (it == "10") {
                            call(1)
                        }
                    }
                }
                parameter("get") {
                    effect {
                        if (it == "file.txt") {
                            call(5)
                        }
                    }
                    parameter("encoding") {
                        effect {
                            call(2)
                        }
                    }
                    toggle("verbose") {
                        effect {
                            call(4)
                        }
                        call(3)
                    }
                }
            }
        }
    }

    @Test
    fun effectWithRemainingTest() {
        arrayOf("--i'm", "--trapped", "--in", "-a", "--universe", "--factory").parseArgs {
            toggle("i'm") {
                effectWithRemaining { rem ->
                    Assertions.assertTrue(
                        rem.containsAll(arrayListOf("--trapped", "--in", "-a", "--universe", "--factory"))
                    )
                }
            }
        }
    }

    @Test
    fun partialParsingTest() {
        CallOrderChecker.create(1) {
            arrayOf("--help", "--me", "--i'm", "--trapped", "--in", "-a", "--universe", "--factory").parseArgs {
                toggle("me") {
                    call(1)
                }
                toggle("wtf") {
                    Assertions.assertTrue(false)
                }
            }
        }
    }

    @Test
    fun testShort() {
        CallOrderChecker.create(3) {
            arrayListOf("-xzftar.tar").parseArgs {
                toggle("extract", 'x') {
                    call(1)
                }
                parameter("file", 'f') {
                    effect {
                        if (it == "tar.tar") {
                            call(2)
                        }
                    }
                }
                toggle("zip", 'z') {
                    call(3)
                }
            }
        }
    }

    @Test
    fun testShortParameterRemove() {
        CallOrderChecker.create(2) {
            arrayListOf("-xftar.tar").parseArgs {
                parameter("file", 'f') {
                    effect {
                        if (it == "tar.tar") {
                            call(1)
                        }
                    }
                }
                toggle("extract", 'x') {
                    call(2)
                }
            }
        }

        CallOrderChecker.create(2) {
            arrayListOf("-xf", "tar.tar").parseArgs {
                parameter("file", 'f') {
                    effect {
                        if (it == "tar.tar") {
                            call(1)
                        }
                    }
                }
                toggle("extract", 'x') {
                    call(2)
                }
            }
        }
    }

    @Test
    fun testParameterRemove() {
        var test = 0

        arrayOf("-xfile.tar").parseArgs {
            parameter("extract", 'x') {
                effectWithRemaining { it, rem ->
                    if (it == "file.tar" && rem.size == 0) {
                        test++
                    }
                }
            }
        }

        Assertions.assertEquals(1, test)

        arrayOf("-x", "file.tar").parseArgs {
            parameter("extract", 'x') {
                effectWithRemaining { it, rem ->
                    if (it == "file.tar" && rem.size == 0) {
                        test++
                    }
                }
            }
        }

        Assertions.assertEquals(2, test)

        arrayOf("--extract", "file.tar").parseArgs {
            parameter("extract", 'x') {
                effectWithRemaining { it, rem ->
                    if (it == "file.tar" && rem.size == 0) {
                        test++
                    }
                }
            }
        }

        Assertions.assertEquals(3, test)

        arrayOf("--extract=file.tar").parseArgs {
            parameter("extract", 'x') {
                effectWithRemaining { it, rem ->
                    if (it == "file.tar" && rem.size == 0) {
                        test++
                    }
                }
            }
        }

        Assertions.assertEquals(4, test)
    }

    @Test
    fun testShortWeirdParameter() {
        CallOrderChecker.create(1) {
            arrayOf("-xgg").parseArgs {
                parameter("get", 'g') {
                    effect {
                        if (it == "g") {
                            call(1)
                        }
                    }
                }
            }
        }
    }

    @Test
    fun testParameterMissing() {
        var test = 0

        try {
            arrayOf("--halp", "--get").parseArgs {
                parameter("get") {}
            }
        } catch (ex: RuntimeException) {
            if (ex.message == "Not enough arguments!") {
                test++
            }
        }

        Assertions.assertEquals(1, test)

        try {
            arrayOf("-xg").parseArgs {
                parameter("get", 'g') {}
            }
        } catch (ex: RuntimeException) {
            if (ex.message == "Not enough arguments!") {
                test++
            }
        }

        Assertions.assertEquals(2, test)
    }
}